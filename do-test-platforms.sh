#!/bin/bash
#
# This script runs tests on all supported platforms
#


## Associative array (Bash 4 feature):
declare -A implementations=(
	["SBCL"]="sbcl --script"
	["Clisp"]="clisp"
	["ECL"]="ecl -load"
	["GCL"]="GCL_ANSI=1 gcl -load"
	['CCL']="ccl64 --load"
	["ABCL"]="java -jar /home/jeronimo/pkg/abcl/dist/abcl.jar --batch --load"
#	["Clasp"]="/home/jeronimo/pkg/clasp/build/clasp --load"
#	["XCL"]="/home/jeronimo/pkg/lisp/xcl/xcl --load"  XCL is currently not buildable
	)

echo "- - - - -"
for name in "${!implementations[@]}";  do
	echo "$name: "
	${implementations["$name"]} do-tests.lisp;
	echo "- - - - -"
done

